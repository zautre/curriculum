
## Abstract Classes
#HSLIDE
As described in the previous example, `polymorphism` is used when you have different derived classes with the same *method*, which has different implementations in each class.
This behavior is achieved through **`virtual`** *methods* that are **`overridden`** in the derived classes.
#HSLIDE
In some situations there is no meaningful need for the **`virtual`** *method* to have a separate definition in the base class.
These *methods* are defined using the ***`abstract`*** keyword and specify that the derived classes must define that *method* on their own.
#HSLIDE
You cannot create objects of a class containing an abstract *method*, which is why the class itself should be abstract.
We could use an abstract *method* in the Shape class:
```c
abstract class Shape {
   public abstract void Draw();
}
```
#HSLIDE
The Shape class itself must be declared abstract because it contains an abstract *method*. 
Abstract *method* declarations are only permitted in abstract classes.
#HSLIDE
>Remember, abstract *method* declarations are only permitted in abstract classes. Members marked as abstract, or included in an abstract class, must be implemented by classes that derive from the abstract class. An abstract class can have multiple abstract members.

#HSLIDE
An abstract class is intended to be a base class of other classes.
Now, having the abstract class, we can derive the other classes and define their own **Draw()** methods: 
```c
abstract class Shape {
  public abstract void Draw();
}
class Circle : Shape {
  public override void Draw() {
    Console.WriteLine("Circle Draw");
  }
}
class Rectangle : Shape {
  public override void Draw() {
    Console.WriteLine("Rect Draw");
  }
}
static void Main(string[] args) {
  Shape c = new Circle();
  c.Draw();
  //Outputs "Circle Draw"
}
```
#HSLIDE
Abstract classes have the following features:
- An `abstract` class cannot be instantiated.
- An `abstract` class may contain abstract *methods* and accessors.
- A non-`abstract` class derived from an abstract class must include actual implementations of all inherited `abstract` *methods* and accessors.

#HSLIDE
>It is not possible to modify an abstract class with the **sealed** modifier because the two modifiers have opposite meanings. The **sealed** modifier prevents a class from being inherited and the abstract modifier requires a class to be inherited.

#HSLIDE
## Interfaces
#HSLIDE
An **`interface`** is a completely abstract class, which contains **only** abstract members.
It is declared using the ***`interface`*** keyword: 
```c
public interface IShape
{
  void Draw();
}
```
#HSLIDE
All members of the ***interface*** are by default abstract, so no need to use the ***`abstract`*** keyword.
Also, all members of an ***interface*** are always `public`, and no access modifiers can be applied to them.
#HSLIDE
>It is common to use the capital letter **I** as the starting letter for an ***interface*** name.
Interfaces can contain properties, methods, etc. but **cannot** contain fields (variables).

#HSLIDE

When a class implements an ***interface***, it must also implement, or define, all of its methods.
The term implementing an ***interface*** is used  to describe the process of creating a class based on an ***interface***.
The ***interface*** simply describes what a class should do.
#HSLIDE
The syntax to implement an ***interface*** is the same as that to derive a class: 
```c
public interface IShape {
  void Draw();
}
class Circle : IShape {
  public void Draw() {
    Console.WriteLine("Circle Draw");
  }
}
static void Main(string[] args) {
  IShape c = new Circle();
  c.Draw();
  //Outputs "Circle Draw"
}
```
Note, that the ***`override`*** keyword is not needed when you implement an ***interface***.
#HSLIDE
>But why use interfaces rather than abstract classes?
A class can inherit from just one base class, but it can implement multiple interfaces!
Therefore, by using interfaces you can include behavior from **multiple** sources in a class.
To implement multiple interfaces, use a comma separated list of interfaces when creating the class: **class A: IShape, IAnimal, etc**.
#HSLIDE
## Nested Classes
#HSLIDE
C# supports **nested** classes: a class that is a member of another class.
For example: 
```c
class Car {
  string name;
  public Car(string nm) {
    name = nm;
    Motor m = new Motor();
  }
  public class Motor {
    // some code
  }
}
```
#HSLIDE
The **Motor** class is nested in the **Car** class and can be used similar to other members of the class.
A nested class acts as a member of the class, so it can have the same access modifiers as other members (`public`, `private`, `protected`).
#HSLIDE
>Just as in real life, objects can contain other objects. For example, a car, which has its own attributes (color, brand, etc.) contains a motor, which as a separate object, has its own attributes (volume, horsepower, etc.). Here, the Car class can have a nested Motor class as one of its members.
#HSLIDE
## Namespaces
#HSLIDE
When you create a blank project, it has the following structure:
```c
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SoloLearn {
  class Program {
    static void Main(string[] args) {
    }
  }
}
```
Note, that our whole program is inside a **`namespace`**. So, what are `namespaces`?
#HSLIDE
`Namespaces` declare a scope that contains a set of related objects.
You can use a `namespace` to organize code elements.
You can define your own `namespaces` and use them in your program.
#HSLIDE
The ***`using`*** keyword states that the program is using a given `namespace`.
For example, we are using the **System** `namespace` in our programs, which is where the class **Console** is defined: 
```c
using System;
...
Console.WriteLine("Hi");
```
Without the **using** statement, we would have to specify the `namespace` wherever it is used: 
```c
System.Console.WriteLine("Hi");
```
#HSLIDE
>The .NET Framework uses `namespaces` to organize its many classes. **System** is one example of a .NET Framework `namespace`.
Declaring your own `namespaces` can help you group your class and *method* names in larger programming projects.
#HSLIDE