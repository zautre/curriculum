
## Generic Methods
#HSLIDE
### Generics
**Generics** allow the reuse of code across different types.
#HSLIDE
For example, let's declare a ***method*** that swaps the values of its two parameters: 
```c
static void Swap(ref int a, ref int b) {
  int temp = a;
  a = b;
  b = temp;
}
```
#HSLIDE
Our **Swap** ***method*** will work only for `integer` parameters.
If we want to use it for other types, for example, doubles or strings, we have to overload it for all the types we want to use it with.
#HSLIDE
Generics provide a flexible mechanism to define a generic type. 
```c
static void Swap<T>(ref T a, ref T b) {
  T temp = a;
  a = b;
  b = temp;
}
```
In the code above, **T** is the name of our generic type.
We can name it anything we want, but **T** is a commonly used name.
#HSLIDE
Our **Swap** ***method*** now takes two parameters of type **T**. We also use the **T** type for our `temp` variable that is used to swap the values.

>Note the brackets in the syntax **<T>**, which are used to define a generic type.
#HSLIDE
### Generic Methods
Now, we can use our **Swap** ***method*** with different types, as in: 
```c
static void Swap<T>(ref T a, ref T b) {
  T temp = a;
  a = b;
  b = temp;
}
static void Main(string[] args) {
  int a = 4, b = 9;
  Swap<int>(ref a, ref b);
  //Now b is 4, a is 9
  string x = "Hello";
  string y = "World";
  Swap<string>(ref x, ref y);
  //Now x is "World", y is "Hello"
}
```
#HSLIDE

>Multiple generic parameters can be used with a single method.
For example: **Func<T, U>** takes two different generic types.

#HSLIDE
### Generic Classes
Generic types can also be used with classes.
#HSLIDE
The most common use for generic classes is with collections of items.
One type of collection is called a stack. Items are "pushed", or added to the collection, and "popped", or removed from the collection.
#HSLIDE
A stack is sometimes called a Last In First Out (LIFO) data structure.
For example: 
```c
class Stack<T> {
  int index=0;
  T[] innerArray = new T[100];
  public void Push(T item) {
    innerArray[index++] = item; 
  }
  public T Pop() {
    return innerArray[--index]; 
  }
  public T Get(int k) { return innerArray[k]; }
}
```
#HSLIDE
The generic class stores elements in an ***array***. As you can see, the generic type **T** is used as the type of the ***array***, the parameter type for the **Push** ***method***, and the return type for the **Pop** and **Get** methods.
#HSLIDE
Now we can create objects of our generic class: 
```c
Stack<int> intStack = new Stack<int>();
Stack<string> strStack = new Stack<string>();
Stack<Person> PersonStack = new Stack<Person>();
```
We can also use the generic class with custom types, such as the custom defined **Person** type.
#HSLIDE
>In a generic class we do not need to define the generic type for its methods, because the generic type is already defined on the class level.

#HSLIDE
Generic class methods are called the same as for any other object: 
```c
Stack<int> intStack = new Stack<int>();
intStack.Push(3);
intStack.Push(6);
intStack.Push(7);
            
Console.WriteLine(intStack.Get(1));
//Outputs 6
```
#HSLIDE
## Collections
#HSLIDE
The .NET Framework provides a number of **generic collection classes**, useful for storing and manipulating data.
These classes are contained in the **System.Collections.Generic** ***namespace***.
#HSLIDE
**List** is one of the commonly used collection classes: 
```c
List<string> colors = new List<string>();
colors.Add("Red");
colors.Add("Green");
colors.Add("Pink");
colors.Add("Blue");
foreach (var color in colors) {
  Console.WriteLine(color);
}
/*Outputs
Red
Green
Pink
Blue
*/
```

We defined a List that stores strings and iterated through it using a **foreach** loop.
#HSLIDE
The List class contains a number of useful methods:
- **Add** adds an element to the List.
- **Clear** removes all elements from the List.
- **Contains** determines whether the specified element is contained in the List.
- **Count** returns the number of elements in the List.
- **Insert** adds an element at the specified index.
- **Reverse** reverses the order of the elements in the List.

#HSLIDE
>So why use Lists instead of arrays?
Because, unlike arrays, the group of objects you work with in a collection can grow and shrink dynamically.

#HSLIDE
Commonly used generic collection types include:
- **Dictionary<TKey, TValue>** represents a collection of key/value pairs that are organized based on the key.
- **List<T>** represents a list of objects that can be accessed by index. Provides methods to search, sort, and modify lists.
- **Queue<T>** represents a first in, first out (FIFO) collection of objects.
- **Stack<T>** represents a last in, first out (LIFO) collection of objects.

#HSLIDE
>Choose the type of collection class based on the data you need to store and the operations you need.
#HSLIDE
## The end
We've reached the end of our course on C#! So, was it helpful? Did you learn something new? What can we actually say about programming on C#?
There are more programming languages to come! Wondering where you can see them? 





























