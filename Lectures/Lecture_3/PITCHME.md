### The if-else Statement

#HSLIDE


The **if** statement is a conditional statement that executes a block of code when a condition is true.
The general form of the **if** statement is:
```c
if (condition)
{
    // Execute this code when condition is true
}
```
The condition can be any expression that returns true or false.

#HSLIDE

For example: 
```c
static void Main(string[] args)
{
   int x = 8;
   int y = 3;
 
   if (x > y)
   { 
      Console.WriteLine("x is greater than y");
   }
}
```

#HSLIDE

>When only one line of code is in the if block, the curly braces can 
be omitted.

For example:

```c
if (x > y)
Console.WriteLine("x is greater than y");
```
#HSLIDE

### Relational Operators
#HSLIDE
Use **relational operators** to evaluate conditions. In addition to the less than (<) and greater than (>) operators, the following operators are available: 

| Operator  | Description  | Example  | Result |  
|---|:---:|:---:|---:|
| >= | Greater than or equal to | 7>=4 | True |
| <= | Less than or equal to | 7<=4 | False |  
| == | Equal to  | 7==4 | False |
| != | Not equal to | 7!=4 | True |

#HSLIDE

**Example:**
```c
if (a == b) {
  Console.WriteLine("Equal");
}
// Displays Equal if the value of a is equal to the value of b
```

#HSLIDE
### The else Clause
#HSLIDE

An optional **else** clause can be specified to execute a block of code when the condition in the **if** statement evaluates to **false**.
**Syntax:** 
```c
if (condition) 
{
   //statements
}
else 
{
   //statements
}
```

#HSLIDE

For example: 
```c
int mark = 85;

if (mark < 50) 
{
   Console.WriteLine("You failed.");
}
else 
{
   Console.WriteLine("You passed.");
}

// Outputs "You passed." 
```

#HSLIDE

### Nested if Statements

#HSLIDE


You can also include, or **nest**, if statements within another if statement.
**For example:** 
```c
int mark = 100;

if (mark >= 50) {
  Console.WriteLine("You passed.");
  if (mark == 100) {
    Console.WriteLine("Perfect!");
  }
}
else {
  Console.WriteLine("You failed.");
}

/*Outputs
You passed.
Perfect!
*/
```

#HSLIDE

You can nest an unlimited number of if-else statements.
**For example:**

```c
int age = 18;
if (age > 14) {
  if(age > 18) {
    Console.WriteLine("Adult");}
  else {
    Console.WriteLine("Teenager");}}
else {
  if (age > 0) {
    Console.WriteLine("Child");}
  else {
    Console.WriteLine("Something's wrong");}}
//Outputs "Teenager"
```

>Remember that all else clauses must have corresponding if statements.


#HSLIDE
### The if-else if Statement
The **if-else if** statement can be used to decide among three or more actions.
**For example:**
```c
int x = 33;

if (x == 8) {
   Console.WriteLine("Value of x is 8");
}
else if (x == 18) {
   Console.WriteLine("Value of x is 18");
}
else if (x == 33) {
   Console.WriteLine("Value of x is 33");
}
else {
   Console.WriteLine("No match");
}
//Outputs "Value of x is 33"
```
#HSLIDE

>Remember, that an if can have zero or more else if's and they must
come before the last else, which is optional.
Once an else if succeeds, none of the remaining else if's or else clause
will be tested!
#HSLIDE
### The Switch Statement
#HSLIDE
The **switch** statement provides a more elegant way to test a variable for equality against a list of values.
Each value is called a **case**, and the variable being switched on is checked for each switch case.
**For example:**
```c
int num = 3;
switch (num)
{
  case 1:
   Console.WriteLine("one");
   break;
  case 2:
   Console.WriteLine("two");
   break;
  case 3:
   Console.WriteLine("three");
   break;
}
//Outputs "three"
```
Each **case** represents a value to be checked, followed by a colon, and the statements to get executed if that case is matched.

>A switch statement can include any number of cases. However,
no two case labels may contain the same constant value.
The break; statement that ends each case will be covered shortly.

#HSLIDE

### The default Case
#HSLIDE

In a switch statement, the optional **default** case is executed when none of the previous cases match.
**Example:** 
```c
int age = 88;
switch (age) {
  case 16:
    Console.WriteLine("Too young");
    break;
  case 42:
    Console.WriteLine("Adult");
    break;
  case 70:
    Console.WriteLine("Senior");
    break;
  default:
    Console.WriteLine("The default case");
    break;
}
// Outputs "The default case"
```

.

#HSLIDE

### The break Statement

#HSLIDE


The role of the **break** statement is to terminate the **switch** statement.
Without it, execution continues past the matching **case** statements and falls through to the next case statements, even when the case labels don’t match the switch variable.
This behavior is called **fallthrough** and modern C# compilers will not compile such code. All case and default code must end with a **break** statement.

>The break statement can also be used to break out of a loop. You will learn about loops in the coming lessons.

#HSLIDE
### The for Loop
#HSLIDE

A **for** loop executes a set of statements a specific number of times, and has the syntax:
```c
for([variable to count iterations];[conditions];[increment])
{
statement(s);
}
```
A counter is declared once in **variables**.
Next, the **condition** evaluates the value of the counter and the body of the loop is executed if the condition is true.
After loop execution, the **increment** statement updates the counter. The condition is again evaluated, and the loop body repeats, and stops when the condition becomes **false**.
#HSLIDE

E.G:
```c
int i;
for( i = 0; i < 10; i++)
{
}
```
This would loop until ***`i`*** is no longer less than ten, increasing i by one each time.
>Note the semicolons in the syntax.

#HSLIDE
Compound arithmetic operators can be used to further control loop iterations.
**For example:**
```c
for (int x = 0; x < 10; x+=3)
{
  Console.WriteLine(x);
}

/* Outputs
0
3
6
9
*/
```
#HSLIDE
You can also decrement the counter:
```c
for (int x = 10; x > 0; x-=2)
{
  Console.WriteLine(x);
}

/* Outputs
10
8
6
4
2*/
```

#HSLIDE

The **int** and **increment** statements may be left out, if not needed, but remember that the semicolons are **`mandatory`**.

For example, the **int** can be left out: 
```c
int x = 10;
for ( ; x > 0; x -= 3)
{
  Console.WriteLine(x);
}
```
#HSLIDE
### The while Loop
#HSLIDE
### while
While loops are very similar to For loops, They are defined like this:
```c
while([conditions to be checked])
{
[Code to execute]
}
```
This allows you to continously repeat a section of code while a condition is satisfied.
#HSLIDE
For example, the following code displays the numbers 1 through 5:

```c
int num = 1;
while(num < 6) 
{
   Console.WriteLine(num);
   num++;
}
/* Outputs
1
2
3
4
5
*/
```
The example above declares a variable equal to 1 (`int` num = 1).
#HSLIDE

The compound arithmetic operators can be used to further control the number of times a loop runs. For example:
```c
int num = 1;
while(num < 6) 
{
   Console.WriteLine(num);
   num+=2;
}
/* Outputs
1
3
5
*/
```

>Without a statement that eventually evaluates the loop condition to false,
the loop will continue indefinitely.

#HSLIDE
We can shorten the previous example, by incrementing the value of **num** right in the condition:
```c
int num = 0;
while(++num < 6) 
   Console.WriteLine(num);
```
#HSLIDE
>What do you think, is there a difference between while(num++ < 6) 
and while(++num < 6)?

#HSLIDE
### Exercise
Print `X` to the console `N` time using a while loop.
#HSLIDE
### The do-while Loop
#HSLIDE
### do-while
A **do-while** loop is similar to a **while** loop, except that a **do-while** loop is guaranteed to execute at least one time.
#HSLIDE
For example: 

```c
int a = 0;
do {
  Console.WriteLine(a);
  a++;
} while(a < 5);

/* Outputs
0
1
2
3
4
*/
```

>Note the semicolon after the while statement.
#HSLIDE
### Exercise
Print the value of variable `X` to the screen `N` number of times.
#HSLIDE
### do-while vs. while
If the condition of the **do-while** loop evaluates to **false**, the statements in the **do** will still run once: 

```c
int x = 42;
do {
  Console.WriteLine(x);
  x++;
} while(x < 10);

// Outputs 42
```
#HSLIDE
>The do-while loop executes the statements at least once, and then tests the condition.
The while loop executes the statement only after testing condition.
#HSLIDE








