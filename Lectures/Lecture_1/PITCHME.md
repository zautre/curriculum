### Welcome to our course

#HSLIDE

In this class we will learn the basic concepts 
of developing a code and making it work. 


#HSLIDE

### Here are some important things to know:

* The course will take 3 months.
* In each week we will have 2 lectures.
* The duration of one lecture will be 2 hours.
* There will be grades, but don't worry we won't be as strict as in school!
* You will have homework, which will be obligatory to be done.

#HSLIDE

Why C#?
- Easy to learn
- Object-oriented 
- Modern
- Full stack

#HSLIDE

### The .NET Framework

#HSLIDE

It is a structure that allows the development process to become less complicated. 

>These concepts might seem complex, but for now just remember that 
applications written in C# use the .NET Framework and its components


#HSLIDE

### Variables

#HSLIDE


Variables have type:
```
int myAge;
```

Can be assigned values:
```
myAge = 25;
```

You can assign the value of a variable when you declare it:
```
int myAge = 25;
```

#HSLIDE

### Variable Types

#HSLIDE

The type of the variable determines what kind of information we are going to store in it.

```
int myAge;
```

#HSLIDE

### Built-in Data Types

#HSLIDE

There are a number of built-in data types in C# . The most common are:

  - **int** - integer.

  - **float** - floating point number.

  - **double** - double-precision version of float.

  - **char** - a single character.

  - **bool** - Boolean that can have only one of two values: True or False.

  - **string** - a sequence of characters.

#HSLIDE

The statements below use C# data types:
```
int myAge = 25;
double pi = 3.14;
char first_letter = 'Z';
bool isOnline = true;
string school = "ZaUtre";
```

#HSLIDE

### The var Keyword

#HSLIDE
We can declare variable of type **var**, but we have to assign value in order to do this.

```
var age = 19;
```
The code above makes the compiler determine the type of the variable.

For example, the following program will cause an error: 
```
var age;
age = 19;
```
#HSLIDE

### Your First C# Program

#HSLIDE

 (Here we will implement a task that suits the purposes of the learning process.)
 
#HSLIDE

### Printing Text

#HSLIDE

To display text to the console window you use the **Console.Write** or **Console.WriteLine** methods. 
```
static void Main(string[] args)
{
   Console.WriteLine("Hello World!");
}
```
#HSLIDE

### Displaying Output

#HSLIDE

We can display variable values to the console window: 
```
static void Main(string[] args)
{
   int x = 10;
   Console.WriteLine(x);
}
// Outputs 10
```

#HSLIDE

To display a **formatted** **string**, use the following syntax:
```
static void Main(string[] args)
{
   int x = 5;
   double y = 10;
 
   Console.WriteLine("x = {0}; y = {1}", x, y);
}
// Output: x = 5; y = 10
```
As you can see, the value of **x** replaced **{0}** and the value of **y** replaced **{1}**.
You can have as many variable placeholders as you need.
(i.e.: {3}, {4}, etc.).


#HSLIDE

### Getting User Input

#HSLIDE

### User Input
You can also prompt the user to enter data and then use the **Console.ReadLine** *method* to assign the input to a **string** variable.
The following example asks the user for a gender and they displays a message that includes the input:
```
static void Main(string[] args)
{
    string gender;
    Console.WriteLine("What is your gender?");
    gender = Console.ReadLine();
    Console.WriteLine("You are {0}",gender);
}
//What is your gender? 
//You are male 
```

#HSLIDE

The **Console.ReadLine()** *method* returns a **string**.

**Convert.ToDouble** 

**Convert.ToBoolean**

For **integer** conversation, there are three alternatives available based on the bit size of the **integer**: **Convert.ToInt16**,  **Convert.ToInt32** and  **Convert.ToInt64**.
The default **int** type in C# is 32-bit.

#HSLIDE

Let's create a program that takes an **integer** as input and displays it in a message:
```
static void Main(string[] args)
{
    int age = Convert.ToInt32(Console.ReadLine());
    Console.WriteLine("You are {0} years old", age);
}
```
>If, in the program above, a non-integer value is entered 
(for examle, letters), the Convert will fail and cause an error.
>

#HSLIDE

### Comments

#HSLIDE

```
Console.WriteLine("Hello");
//Prints Hello
```

What is about to happen when we execute the program above?
#HSLIDE


### Multi-Line Comments
Comments that require multiple lines begin with ```/*``` and end with ```*/``` at the end of the comment block.
You can place them on the same line or insert one or more lines between them.
```
/* Some long 
   multi-line
   comment text
*/
int x = 4;
Console.WriteLine(x);
```
#HSLIDE
### What we've learned!
* Why we use variables?
* What is a comment?
* How can we display a result from our program?
* How should a program look alike in order to work properly?

#HSLIDE