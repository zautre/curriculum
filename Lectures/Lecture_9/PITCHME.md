
## Arrays & Strings
#HSLIDE
## Arrays
#HSLIDE
C# provides numerous built-in classes to store and manipulate data.
One example of such a class is the **Array** class.
#HSLIDE
An *array* is a data structure that is used to store a collection of data.
You can think of it as a collection of variables of the **same type**.
#HSLIDE
For example, consider a situation where you need to store 100 numbers.
Rather than declare 100 different variables, you can just declare an *array* that stores 100 **elements**.
#HSLIDE
To declare an *array*, specify its element types with square brackets: 
```c
int[ ] myArray;
```
This statement declares an *array* of integers.
#HSLIDE
Since *arrays* are **objects**, we need to instantiate them with the ***`new`*** keyword: 
```c
int[ ] myArray = new int[5]; 
```

>Note the square brackets used to define the number of elements the array
should hold.

#HSLIDE

After creating the *array*, you can assign values to individual elements by using the **index number**: 
```c
int[ ] myArray = new int[5];
myArray[0] = 23;
```
This will assign the value 23 to the first element of the *array*.
#HSLIDE

>Arrays in C# are zero-indexed meaning the first member has index ***0***,
the second has index 1, and so on!

#HSLIDE

- We can provide initial values to the **array** when it is declared by using curly brackets:

```c
string[ ] names = new string[3] {"John", "Mary", "Jessica"};
double[ ] prices = new double[4] {3.6, 9.8, 6.4, 5.9};
```
#HSLIDE
- We can omit the size declaration when the number of elements are provided in the curly braces: 

```c
string[ ] names = new string[ ] {"John", "Mary", "Jessica"};
double[ ] prices = new double[ ] {3.6, 9.8, 6.4, 5.9};
```
#HSLIDE
- We can even omit the `new` operator. The following statements are identical to the ones above: 

```c
string[ ] names = {"John", "Mary", "Jessica"};
double[ ] prices = {3.6, 9.8, 6.4, 5.9};
```
#HSLIDE

>Array values should be provided in a comma separated list enclosed in {curly braces}.

#HSLIDE
As mentioned, each element of an *array* has an index number.
For example, consider the following *array*:
```c
int[ ] b = {11, 45, 62, 70, 88};
```
#HSLIDE
The elements of `b` have the following indexes: 

| 11 | 45 | 62 | 70 | 88 |
|----|----|----|----|----|
| [0]| [1]| [2]| [3]| [4]|

#HSLIDE
To access individual *array* elements, place the element's index number in square brackets following the *array* name. 
```c
Console.WriteLine(b[2]);
//Outputs 62
Console.WriteLine(b[3]);
//Outputs 70
```
#HSLIDE
## Using Arrays in Loops
#HSLIDE
### Arrays & Loops
It's occasionally necessary to iterate through the elements of an *array*, making element assignments based on certain calculations.
This can be easily done using loops.
#HSLIDE
For example, you can declare an *array* of 10 integers and assign each element an even value with the following loop:
```c
int[ ] a = new int[10];
for (int k = 0; k < 10; k++) {
  a[k] = k*2;
}
```
#HSLIDE
We can also use a loop to read the values of an *array*.
For example, we can display the contents of the *array* we just created:
```c
for (int k = 0; k < 10; k++) {
  Console.WriteLine(a[k]);
}
```
#HSLIDE
### The foreach Loop
The **foreach** loop provides a shorter and easier way of accessing *array* elements.
#HSLIDE
The previous example of accessing the elements could be written using a **foreach** loop:
```c
foreach (int k in a) {
  Console.WriteLine(k);
}
```
#HSLIDE

>The data type of the variable in the foreach loop should match the type
of the array elements.
Often the keyword var is used as the type of the variable, as in: foreach
(var k in a). The compiler determines the appropriate type for var.

#HSLIDE
## Multidimensional Arrays
#HSLIDE
An *array* can have multiple dimensions. A **multidimensional array** is declared as follows:
```c
type[, , … ,] arrayName = new type[size1, size2, …, sizeN];
```
#HSLIDE
For example, let's define a two-dimensional 3x4 *integer array*: 
```c
int[ , ] x = new int[3,4];
```
#HSLIDE
Visualize this *array* as a table composed of 3 rows and 4 columns: 

|         | `Column 1` | `Column 2` | `Column 3` | `Column 4` |
|---------|------------|------------|------------|------------|
| `Row 1` |   x[0][0]  |   x[0][1]  |   x[0][2]  |   x[0][3]  |
| `Row 2` |   x[1][0]  |   x[1][1]  |   x[1][2]  |   x[1][3]  |
| `Row 3` |   x[2][0]  |   x[2][1]  |   x[2][2]  |   x[2][3]  |

#HSLIDE
We can initialize multidimensional arrays in the same way as single-dimensional arrays.
For example:
```c
int[ , ] someNums = { {2, 3}, {5, 6}, {4, 6} };
```
#HSLIDE
To access an element of the *array*, provide both indexes. For example `someNums[2, 0]` will return the value **4**, as it accesses the first column of the third row.
#HSLIDE
Let's create a program that will display the values of the *array* in the form of a table. 
```c
for (int k = 0; k < 3; k++) {
  for (int j = 0; j < 2; j++) {
    Console.Write(someNums[k, j]+" ");
  }
  Console.WriteLine();
}
```
#HSLIDE

>Arrays can have any number of dimensions, but keep in mind that arrays with more than three dimensions are harder to manage.
#HSLIDE