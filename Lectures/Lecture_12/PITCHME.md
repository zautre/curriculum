## this & readonly
#HSLIDE
### The this Keyword
The ***`this`*** keyword is used inside the class and refers to the current *instance* of the class, meaning it refers to the current object.
#HSLIDE
One of the common uses of **this** is to distinguish class members from other data, such as local or formal parameters of a *method*, as shown in the following example: 
```c
class Person {
    private string name;
    public Person(string name) {
    this.name = name;
  }
}
```
#HSLIDE
Here, `this.name` represents the member of the class, whereas `name` represents the parameter of the *constructor*.

>Another common use of **this** is for passing the current instance to a method as parameter: ShowPersonInfo(**this**);
#HSLIDE
### The readonly Modifier
The **readonly** modifier prevents a member of a class from being modified after construction.
It means that the field declared as **readonly** can be modified only when you declare it or from within a *constructor*.
#HSLIDE
For example:
```c
class Person {
  private readonly string name = "John"; 
  public Person(string name) {
    this.name = name; 
  }
}
```
If we try to modify the `name` field anywhere else, we will get an error.
There are three major differences between **readonly** and **const** fields.
#HSLIDE
First, a constant field must be initialized when it is declared, whereas a readonly field can be declared without initialization, as in: 
```c
readonly string name; // OK
const double PI; // Error
```
Second, a **readonly** field value can be changed in a *constructor*, but a constant value cannot.
#HSLIDE
Third, the **readonly** field can be assigned a value that is a result of a calculation, but constants cannot, as in: 
```c
readonly double a = Math.Sin(60); // OK
const double b = Math.Sin(60); // Error! 
```
#HSLIDE
## Indexers
#HSLIDE
An **indexer** allows objects to be indexed like an *array*.
As discussed earlier, a *string* variable is actually an object of the **String** class.
#HSLIDE
Further, the String class is actually an array of Char objects.
In this way, the *string* class implements an **indexer** so we can access any character (Char object) by its index:
#HSLIDE
```c
string str = "Hello World";
char x = str[4];
Console.WriteLine(x);
//Outputs "o"
```
#HSLIDE
>Arrays use ***integer*** indexes, but indexers can use any type of index, such as strings, characters, etc.
#HSLIDE

Declaration of an **indexer** is to some extent similar to a `property`. The difference is that **indexer** accessors require an ***index***.
Like a `property`, you use **get** and **set** accessors for defining an **indexer**. 
#HSLIDE
However, where properties return or set a specific data member, indexers return or set a particular value from the object *instance*.
Indexers are defined with the ***`this`*** keyword.
#HSLIDE
For example:
```c
class Clients {
  private string[] names = new string[10];
  public string this[int index] {
    get {
      return names[index];
    }
    set {
      names[index] = value;
    }
  }
}
```
#HSLIDE
As you can see, the **indexer** definition includes the ***`this`*** keyword and an index, which is used to get and set the appropriate value.
Now, when we declare an object of class Clients, we use an index to refer to specific objects like the elements of an *array*: 
#HSLIDE
```c
Clients c = new Clients();
c[0] = "Dave";
c[1] = "Bob";
Console.WriteLine(c[1]);
//Outputs "Bob"  
```

>You typically use an ***indexer*** if the class represents a list, collection, or ***array*** of objects.

#HSLIDE
## Operator Overloading
#HSLIDE
Most operators in C# can be **overloaded**, meaning they can be redefined for custom actions.
#HSLIDE
For example, you can redefine the action of the plus (+) operator in a custom class.
Consider the **Box** class that has **Height** and **Width** properties: 
```c
class Box {
  public int Height {get; set;}
  public int Width {get; set;}
  public Box(int h, int w) {
    Height = h;
    Width = w;
  }
}
static void Main(string[] args) {
  Box b1 = new Box(14, 3);
  Box b2 = new Box(5, 7);
}
```
#HSLIDE
We would like to add these two **Box** objects, which would result in a new, bigger Box.
So, basically, we would like the following code to work: 
```c
Box b3 = b1 + b2;
```
The **Height** and **Width** properties of object b3 should be equal to the sum of the corresponding properties of the b1 and b2 objects.
#HSLIDE

>This is achieved through **operator overloading**.

#HSLIDE

Overloaded operators are methods with special names, where the keyword ***`operator`*** is followed by the symbol for the operator being defined.
Similar to any other *method*, an overloaded operator has a return type and a parameter list.
#HSLIDE
For example, for our **Box** class, we overload the + operator:
```c
public static Box operator+ (Box a, Box b) {
  int h = a.Height + b.Height;
  int w = a.Width + b.Width;
  Box res = new Box(h, w);
  return res;
}
```
#HSLIDE
The overloaded operator must be **`static`**.
Putting it all together: 
```c
class Box {
  public int Height { get; set; }
  public int Width { get; set; }
  public Box(int h, int w) {
    Height = h;
    Width = w;
  }
  public static Box operator+(Box a, Box b) {
    int h = a.Height + b.Height;
    int w = a.Width + b.Width;
    Box res = new Box(h, w);
    return res;
  }
}
static void Main(string[] args) {
  Box b1 = new Box(14, 3);
  Box b2 = new Box(5, 7);
  Box b3 = b1 + b2;
  Console.WriteLine(b3.Height); //19
  Console.WriteLine(b3.Width); //10
}
```
#HSLIDE
>All arithmetic and comparison operators can be overloaded. For instance, you could define greater than and less than operators for the boxes that would compare the Boxes and return a **boolean** result. Just keep in mind that when overloading the greater than operator, the less than operator should also be defined.

#HSLIDE