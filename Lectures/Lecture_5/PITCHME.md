#HSLIDE
## Introduction to methods
#HSLIDE
### What is a Method?
Methods in C# are portions of a larger program that perform specific tasks.
They can be used to keep code clean by seperating it into seperate pieces. 
In C# methods can be in the main program or can be in libraries, which are external files,
containing classes and subroutines which can be imported into a program.
In addition to the C# built-in methods, you may also define your own.
#HSLIDE
Methods have many advantages, including:
- Reusable code.
- Easy to test.
- Modifications to a **method** do not affect the calling program.
- One **method** can accept many different inputs.

`Every valid C# program has at least one method, the Main method!`
#HSLIDE
### Declaring Methods
To use a **method**, you need to declare the **method** and then call it.
Each **method** declaration includes:
- the return type
- the **method** name
- an optional list of parameters.

#HSLIDE
Here you can see the exact syntax for declaring methods:
```c
<return type> name(type1 par1, type2 par2, … , typeN parN)
{
    List of statements
}
```
#HSLIDE
For example, the following **method** has an `int` parameter and returns the number squared: 
```c
int Sqr(int x)
{
  int result = x*x;
  return result;
}
```
#HSLIDE
The **return** type of a *method* is declared before its name. In the example above, the return type is `int`, which indicates that the *method* returns an *integer value*. When returning a value, we must include a **return** statement. *Methods* that return a value are often used in assignment statements.
Methods that do not return a value after an operation have return value **void**. In this case, the *method* cannot be called as part of an assignment statement.
#HSLIDE

>void is a basic data type that defines a valueless state.

#HSLIDE
Here is another example of the usage of *methods*:
```c
public static int Multiply(int a, int b)
{
    return a * b;
}
```
This method has been passed two parameters, **integer** `a` and **integer** `b`, this is how you provide input for a subroutine (method).
#HSLIDE
### Calling Methods
Parameters are optional; that is, you can have a *method* with no parameters.
As an example, let's define a *method* that does not return a value, and just prints a line of text to the screen. 
#HSLIDE
```c
static void SayHi()
{
  Console.WriteLine("Hello");
}
```
Our *method*, entitled `SayHi`, returns **void**, and has no parameters.
To execute a *method*, you simply call the *method* by using the name and any required arguments in a statement. 
#HSLIDE

>The static keyword will be discussed later; it is used to make methods
accessible in Main.
#HSLIDE
## Method Parameters
#HSLIDE
### Parameters
Method declarations can define a list of **parameters** to work with.
Parameters are variables that accept the values passed into the *method* when called.
#HSLIDE
For example: 
```c
void Print(int x) 
{
  Console.WriteLine(x);
}
```
This defines a *method* that takes one `integer` parameter and displays its value.
#HSLIDE

>Parameters behave within the method similarly to other local variables.
They are created upon entering the method and are destroyed 
upon exiting the method.
#HSLIDE
Now you can call the *method* in Main and pass in the value for its parameters (also called **arguments**): 
```c
static void Print(int x) 
{
  Console.WriteLine(x);
}
static void Main(string[] args)
{
  Print(42);
}
```
#HSLIDE

>The value 42 is passed to the method as an argument and is assigned to 
the formal parameter x.
#HSLIDE
You can pass different arguments to the same *method* as long as they are of the expected type.
For example: 
```c
static void Func(int x)
{
  Console.WriteLine(x*2);
}
static void Main(string[] args)
{
  Func(5);
  //Outputs 10
  Func(12);
  //Outputs 24
  Func(42);
  //Outputs 84
}
```
#HSLIDE
## Multiple Parameters
#HSLIDE
You can have as many parameters as needed for a *method* by separating them with **commas** in the definition.
#HSLIDE
Let's create a simple *method* that returns the sum of two parameters: 
```c
int Sum(int x, int y)
{
   return x+y;
}
```
The **Sum** *method* takes two integers and returns their sum. This is why the return type of the *method* is `int`. Data **type** and **name** should be defined for each parameter.
#HSLIDE

>Methods return values using the return statement.

#HSLIDE
A *method* call with multiple parameters must separate arguments with **commas**.
For example, a call to **Sum** requires two arguments:
```c
static void Main(string[] args)
{
  Console.WriteLine(Sum(8, 6));
  // Outputs 14
}
```
#HSLIDE
In the call above, the return value was displayed to the console window. Alternatively, we can assign the return value to a variable, as in the code below:
```c
static void Main(string[] args)
{
  int res = Sum(11, 42);
  Console.WriteLine(res);
  //Outputs 53
}
```
#HSLIDE

>You can add as many parameters to a single method as you want. If you
have multiple parameters, remember to separate them with commas,
both when declaring them and when calling the method.
#HSLIDE



## Optional & Named Arguments 
#HSLIDE
### Optional Arguments
When defining a *method*, you can specify a **default value** for optional parameters. Note that optional parameters must be defined after required parameters. If corresponding arguments are missing when the *method* is called, the *method* uses the default values.
#HSLIDE
To do this, assign values to the parameters in the *method* definition, as shown in this example. 
```c
static int Pow(int x, int y=2)
{
  int result = 1;
  for (int i = 0; i < y; i++)
  {
    result *= x;
  }
  return result;
}
```
The Pow *method* assigns a default value of 2 to the y parameter. If we call the *method* without passing the value for the y parameter, the default value will be used.
#HSLIDE

>As you can see, default parameter values can be used for calling 
the same method in different situations without requiring arguments
for every parameter.
Just remember, that you must have the parameters with default values at the end of the parameter list when defining the method.

#HSLIDE
### Named Arguments
Named arguments free you from the need to remember the order of the parameters in a *method* call. Each **argument** can be specified by the matching parameter name.
#HSLIDE
For example, the following *method* calculates the area of a rectangle by its height and width: 
```c
static int Area(int h, int w)
{
  return h * w;
}
```
#HSLIDE
When calling the *method*, you can use the parameter names to provide the arguments in any order you like: 
```c
static void Main(string[] args)
{
  int res = Area(w: 5, h: 8);
  Console.WriteLine(res);
  //Outputs 40
}
```
#HSLIDE

>Named arguments use the name of the parameter followed by a colon
and the value.

#HSLIDE