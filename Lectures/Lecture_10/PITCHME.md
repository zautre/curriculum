
## Jagged Arrays
#HSLIDE
A **jagged array** is an *array* whose elements are arrays. So it is basically an **`array` of arrays**.
#HSLIDE
The following is a declaration of a single-dimensional *array* that has three elements, each of which is a single-dimensional *array* of integers: 
```c
int[ ][ ] jaggedArr = new int[3][ ];
```
#HSLIDE
Each dimension is an *array*, so you can also initialize the *array* upon declaration like this: 
```c
int[ ][ ] jaggedArr = new int[ ][ ] 
{
  new int[ ] {1,8,2,7,9},
  new int[ ] {2,4,6},
  new int[ ] {33,42}
};
```
#HSLIDE
You can access individual *array* elements as shown in the example below: 
```c
int x = jaggedArr[2][1]; //42
```
#HSLIDE

>A jagged array is an array-of-arrays, so an int[ ][ ] is an array of int[ ], each of which can be of different lengths and occupy their own
block in memory.
A multidimensional array (int[,]) is a single block of memory (essentially a matrix). It always has the same amount of columns for every row.

#HSLIDE
## Array Properties & Methods
#HSLIDE
### Arrays Properties
The Array class in C# provides various properties and methods to work with arrays.
#HSLIDE
For example, the ***`Length`*** and ***`Rank`*** properties return the number of elements and the number of dimensions of the *array*, respectively.
You can access them using the dot syntax, just like any class members:
```c
int[ ] arr = {2, 4, 7};
Console.WriteLine(arr.Length); 
//Outputs 3
Console.WriteLine(arr.Rank); 
//Outputs 1
```
#HSLIDE
The **`Length`** `property` can be useful in **for** loops where you need to specify the number of times the loop should run.
For example: 
```c
int[ ] arr = {2, 4, 7};
for(int k=0; k<arr.Length; k++) {
  Console.WriteLine(arr[k]);
}
```
#HSLIDE
### Array Methods
There are a number of methods available for arrays:
* **Max** returns the largest value.
* **Min** returns the smallest value.
* **Sum** returns the sum of all elements.

#HSLIDE
For example:
```c
int[ ] arr = { 2, 4, 7, 1};
Console.WriteLine(arr.Max());
//Outputs 7
Console.WriteLine(arr.Min());
//Outputs 1
Console.WriteLine(arr.Sum());
//Outputs 14
```
#HSLIDE

>C# also provides a static Array class with additional methods. 
You will learn about those in the next module.

#HSLIDE
## Working with Strings
#HSLIDE
### Strings
It’s common to think of strings as arrays of characters. In reality, strings in C# are objects.
When you declare a *string* variable, you basically instantiate an object of type `String`.
#HSLIDE
String objects support a number of useful properties and methods:
* **`Length`** returns the length of the *string*.
* **`IndexOf(value)`** returns the index of the first occurrence of the value within the *string*.
* **`Insert(index, value)`** inserts the value into the *string* starting from the specified index.

#HSLIDE
* **`Remove(index)`** removes all characters in the *string* after the specified index.
* **`Replace(oldValue, newValue)`** replaces the specified value in the *string*.
* **`Substring(index, length)`** returns a substring of the specified length, starting from the specified index. If length is not specified, the operation continues to the end of the *string*.
* **`Contains(value)`** returns true if the *string* contains the specified value.

#HSLIDE
The examples below demonstrate each of the String members: 
```c
string a = "some text";
Console.WriteLine(a.Length);
//Outputs 9
Console.WriteLine(a.IndexOf('t'));
//Outputs 5
 a = a.Insert(0, "This is ");
Console.WriteLine(a);
//Outputs "This is some text"
a = a.Replace("This is", "I am");
Console.WriteLine(a);
//Outputs "I am some text"
if(a.Contains("some"))
  Console.WriteLine("found");
//Outputs "found"
a = a.Remove(4);
Console.WriteLine(a);
//Outputs "I am"
a = a.Substring(2);
Console.WriteLine(a);
//Outputs "am"
```
#HSLIDE
You can also access characters of a *string* by its index, just like accessing elements of an *array*:
```c
string a = "some text";
Console.WriteLine(a[2]);
//Outputs "m"
```

>Indexes in strings are similar to arrays, they start from 0.

#HSLIDE
### Working with Strings
Let's create a program that will take a *string*, replace all occurrences of the word "dog" with "cat" and output the first sentence only. 
#HSLIDE
```c
string text = "This is some text about a dog. The word dog appears in this text a number of times. This is the end.";
text = text.Replace("dog", "cat");
text = text.Substring(0, text.IndexOf(".")+1);
Console.WriteLine(text);
//Outputs: "This is some text about a cat."
```
#HSLIDE

>C# provides a solid collection of tools and methods to work and manipulate
strings. You could, for example, find the number of times a specific word
appears in a book with ease, using those methods.

#HSLIDE