## More on classes
#HSLIDE
## Destructors
#HSLIDE
As constructors are used when a class is instantiated, **destructors** are automatically invoked when an object is destroyed or deleted.
#HSLIDE
Destructors have the following attributes:
- A class can only have **one** *destructor*.
- *Destructors* cannot be called. They are invoked automatically.
- A *destructor* does not take modifiers or have parameters.
- The name of a *destructor* is exactly the same as the class prefixed with a **tilde (~)**.

#HSLIDE
For examle:
```c
class Dog
{
  ~Dog() 
  {
    // code statements
  }
}
```

>Destructors can be very useful for releasing resources before coming out of the program. This can include closing files, releasing memory, and so on.


#HSLIDE
Let’s include WriteLine statements in the *destructor* and *constructor* of our class and see how the program behaves when an object of that class is created and when the program ends: 
```c
class Dog
{
  public Dog() {
    Console.WriteLine("Constructor");
  }
  ~Dog() {
    Console.WriteLine("Destructor");
  }
}
static void Main(string[] args) {
  Dog d = new Dog();
}
/*Outputs:
Constructor
Destructor
/*
```
#HSLIDE
## Static Members
#HSLIDE
### Static
Now it's time to discuss the **`static`** keyword.
You first noticed it in the Main *method*'s declaration:
```c
static void Main(string[] args)
```
#HSLIDE
Class members (variables, properties, methods) can also be declared as **`static`**.
This makes those members belong to the class itself, instead of belonging to individual objects.
#HSLIDE
No matter how many objects of the class are created, there is only **one** copy of the `static` member.
For example:
```c
class Cat {
  public static int count=0;
  public Cat() {
    count++;
  }
}
```
#HSLIDE

>No matter how many **Cat** objects are instantiated, there is always only one **count** variable that belongs to the **Cat** class because it was declared `static`.

#HSLIDE
Because of their global nature, `static` members can be accessed directly using the **class name** without an object.
For example:
```c
class Cat {
  public static int count=0;
  public Cat() {
    count++;
  }
}
static void Main(string[] args)
{
  Cat c1 = new Cat();
  Cat c2 = new Cat();
  Console.WriteLine(Cat.count);
}
//Outputs 2
```
#HSLIDE

>You must access `static` members using the class name. If you try to access them via an object of that class, you will generate an error.

#HSLIDE
### Static Methods
The same concept applies to `static` methods.
For example: 
```c
class Dog
{
  public static void Bark() {
    Console.WriteLine("Woof");
  }
}
static void Main(string[] args)
{
  Dog.Bark();
}
// Outputs "Woof"
```
#HSLIDE
Static methods can access **only** `static` members. 

>The Main **method** is `static`, as it is the starting point of any program. Therefore any **method** called directly from Main had to be `static`.

#HSLIDE

***
**Constant** members are `static` by definition.
For example:
```c
class MathClass {
  public const int ONE = 1;
}
static void Main(string[] args) {
  Console.Write(MathClass.ONE);
}
//Outputs 1
```
As you can see, we access the `property` **ONE** using the name of the class, just like a `static` member. This is because all **const** members are `static` by default.

#HSLIDE

### Static Constructors
Constructors can be declared `static` to initialize `static` members of the class.
The `static constructor` is automatically called once when we access a `static` member of the class.
For example: 
```c
class SomeClass {
  public static int X { get; set; }
  public static int Y { get; set; }
  static SomeClass() {
    X = 10;
    Y = 20;
  }
}
```

The constructor will get called once when we try to access SomeClass.X or SomeClass.Y. 

#HSLIDE

### Static Classes
An entire class can be declared as `static`.
A `static` **class** can contain only `static` members.
You cannot instantiate an object of a `static` class, as only one *instance* of the `static` class can exist in a program.
Static classes are useful for combining logical properties and methods. A good example of this is the **Math** class.
It contains various useful properties and methods for mathematical operations.

#HSLIDE

For example, the **Pow** *method* raises a number to a power: 
```c
Console.WriteLine(Math.Pow(2, 3));
//Outputs 8
```
You access all members of the Math class using the class name, without declaring an object.
***

#HSLIDE

There are a number of useful `static` *methods* and properties available in C#:

**Math**
* Math.**PI** the constant PI.
* Math.**E** represents the natural logarithmic base e.
* Math.**Max()** returns the larger of its two arguments.
* Math.**Min()** returns the smaller of its two arguments.
* Math.**Abs()** returns the absolute value of its argument.

#HSLIDE

* Math.**Sin()** returns the sine of the specified angle.
* Math.**Cos()** returns the cosine of the specified angle.
* Math.**Pow()** returns a specified number raised to the specified power.
* Math.**Round()** rounds the decimal number to its nearest integral value.
* Math.**Sqrt()** returns the square root of a specified number.

#HSLIDE

**Array**
The **Array** class includes some `static` methods for manipulating arrays: 
```c
int[] arr = {1, 2, 3, 4};
Array.Reverse(arr);
//arr = {4, 3, 2, 1}
Array.Sort(arr);
//arr = {1, 2, 3, 4}
```

#HSLIDE

**String**
```c
string s1 = "some text";
string s2 = "another text";
String.Concat(s1, s2); // combines the two strings
String.Equals(s1, s2); // returns false
```

#HSLIDE

**DateTime**
The **DateTime** structure allows you to work with dates. 
```c
DateTime.Now; // represents the current date & time
DateTime.Today; // represents the current day
DateTime.DaysInMonth(2016, 2); 
//return the number of days in the specified month 
```

#HSLIDE

>The **Console** class is also an example of a `static` class. We use its `static` **WriteLine()** *method* to output to the screen, or the `static` **ReadLine()** *method* to get user input.
The **Convert** class used to convert value types is also a `static` class.