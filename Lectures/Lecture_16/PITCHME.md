## Exception Handling
#HSLIDE
### Exceptions
An **`exception`** is a problem that occurs during program execution. Exceptions cause abnormal termination of the program.
#HSLIDE
An **exception** can occur for many different reasons. Here are some examples:
- A user has entered invalid data.
- A file that needs to be opened cannot be found.
- A network connection has been lost in the middle of communications.
- Insufficient memory and other issues related to physical resources.

#HSLIDE
For example, the following code will produce an exception when run because we request an index which does not exist: 
```c
int[] arr = new int[] { 4, 5, 8 };
Console.Write(arr[8]);
```

>As you can see, exceptions are caused by user error, programmer error, or physical resource issues. However, a well-written program should handle all possible exceptions.

#HSLIDE
### Handling Exceptions
C# provides a flexible mechanism called the **try-catch** statement to handle exceptions so that a program won't crash when an error occurs.
#HSLIDE
The try and catch blocks are used similar to: 
```c
try {
  int[] arr = new int[] { 4, 5, 8 };
  Console.Write(arr[8]);
}
catch(Exception e) {
  Console.WriteLine("An error occurred");
}
//Outputs "An error occurred"
```
#HSLIDE
The code that might generate an ***exception*** is placed in the **try** block.
If an exception occurs, the catch blocks is executed without stopping the program.
#HSLIDE
The type of ***exception*** you want to catch appears in parentheses following the keyword ***`catch`***.
We use the general Exception type to handle all kinds of exceptions. 
#HSLIDE
We can also use the exception object ***e*** to access the exception details, such as the original error message (**e.Message**): 
```c
try {
  int[] arr = new int[] { 4, 5, 8 };
  Console.Write(arr[8]);
}
catch(Exception e) {
  Console.WriteLine(e.Message);
}
// Index was outside the bounds of the array.
```
#HSLIDE
>You can also catch and handle different exceptions separately.
#HSLIDE
### Handling Multiple Exceptions
A single `try` block can contain multiple `catch` blocks that handle different exceptions separately.
Exception handling is particularly useful when dealing with user input.
#HSLIDE
For example, for a program that requests user input of two numbers and then outputs their quotient, be sure that you handle division by zero, in case your user enters 0 as the second number. 
```c
int x, y;
try {
  x = Convert.ToInt32(Console.Read());
  y = Convert.ToInt32(Console.Read());
  Console.WriteLine(x / y);
}
catch (DivideByZeroException e) {
  Console.WriteLine("Cannot divide by 0");
}
catch(Exception e) {
  Console.WriteLine("An error occurred");
}
```
#HSLIDE
Now, if the user enters 0 for the second number, "Cannot divide by 0" will be displayed.
If, for example, the user enters ***non-integer*** values, "An error occurred" will be displayed.
#HSLIDE
>The following exception types are some of the most commonly used: FileNotFoundException, FormatException, IndexOutOfRangeException, InvalidOperationException, OutOfMemoryException.
#HSLIDE
### finally
An optional **`finally`** block can be used after the `catch` blocks. The `finally` block is used to execute a given set of statements, whether an exception is thrown or not.
#HSLIDE
For example: 
```c
int result=0;
int num1 = 8;
int num2 = 4;
try {
  result = num1 / num2;
}
catch (DivideByZeroException e) {
  Console.WriteLine("Error");
}
finally {
  Console.WriteLine(result);
}
```
#HSLIDE
>The **`finally`** block can be used, for example, when you work with files or other resources. These should be closed or released in the `finally` block, whether an exception is raised or not.
#HSLIDE
## Working with Files
#HSLIDE
### Writing to Files
The **System.IO** **namespace** has various classes that are used for performing numerous operations with files, such as creating and deleting files, reading from or writing to a file, closing a file, and more.
#HSLIDE
The **File** class is one of them. For example: 
```c
string str = "Some text";
File.WriteAllText("test.txt", str);
```
#HSLIDE
The **WriteAllText()** ***method*** creates a file with the specified path and writes the content to it. If the file already exists, it is overwritten.
The code above creates a file **test.txt** and writes the contents of the **str** `string` into it.
#HSLIDE
>To use the **File** class you need to use the System.IO ***namespace***: using System.IO;
#HSLIDE
### Reading from Files
You can read the content of a file using the **ReadAllText** ***method*** of the File class: 
```c
string txt = File.ReadAllText("test.txt");
Console.WriteLine(txt); 
```
This will output the content of the test.txt file.
#HSLIDE
The following ***methods*** are available in the **File** class:
- **AppendAllText()** - appends text to the end of the file.
- **Create()** - creates a file in the specified location.
- **Delete()** - deletes the specified file.
- **Exists()** - determines whether the specified file exists.
- **Copy()** - copies a file to a new location.
- **Move()** - moves a specified file to a new location

#HSLIDE
>All methods automatically close the file after performing the operation.

#HSLIDE