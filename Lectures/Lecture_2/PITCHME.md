### Constants

#HSLIDE

**Constants** store a value that cannot be changed.
To declare a constant, use the **const** modifier.
For example: 
```
const double PI = 3.14; 
```
The value of **const** PI cannot be changed during program execution.

For example, an assignment statement later in the program will cause an error:

```
const double PI = 3.14;
PI = 7; //error
```
Constants must be initialized with a value when declared!

#HSLIDE

### Operators

#HSLIDE


C# supports the following arithmetic operators: 


| Operator |Symbol|Form|
|----------|:-------------:|------:|
| Addition |+|**x + y**|
| Substraction |-|**x - y**|
| Multiplication |*| **x * y**|
| Division |/| **x / y**|
|Modulus|%|**x % y**|


#HSLIDE

For example: 
```
int x = 10;
int y = 4;
Console.WriteLine(x-y);
//Outputs 6
```

#HSLIDE

### Division

#HSLIDE


**Example:** 

```
int x = 10 / 4;
Console.WriteLine(x);
// Outputs 2
```
>Division by 0 is undefined and will crash your program!

#HSLIDE

### Modulus

#HSLIDE

The modulus operator (%) is informally known as the remainder operator because it returns the remainder of an **integer** division.
**For example:**
```
int x = 9 % 4;
Console.WriteLine(x);
// Outputs 1
```
You can use it to find last digits.
```
        int num = 4965721;
        Console.WriteLine("The number is: " + num);
        Console.WriteLine();
 
        int takeLastOneDigit = num % 10;
        Console.WriteLine("The last ONE digit is: " + takeLastOneDigit);
        // output 
        //The number is: 4965721 
        //The last ONE digit is: 1 
 ```
#HSLIDE

### Operator Precedence

#HSLIDE

**Operator precedence** determines the grouping of terms in an expression, which affects how an expression is evaluated. Certain operators take higher precedence over others; for example, the multiplication operator has higher precedence than the addition operator.

#HSLIDE

**For example:**
```
int x = 4+3*2;
Console.WriteLine(x);
// Outputs 10
```

The program evaluates 3*2 first, and then adds the result to 4.
As in mathematics, using **parentheses** alters operator precedence.

```
int x = (4 + 3) *2;
Console.WriteLine(x);
// Outputs 14
```

#HSLIDE

### Assignment Operators

#HSLIDE

The **= assignment** operator assigns the value on the right side of the operator to the variable on the left side.

C# also provides **compound assignment operators** that perform an operation and an assignment in one statement.
**For example:**
```
int x = 42;
x += 2; // equivalent to x = x + 2
x -= 6; // equivalent to x = x - 6
```

#HSLIDE

The same shorthand syntax applies to the multiplication, division, and modulus operators.
```
x *= 8; // equivalent to x = x * 8
x /= 5; // equivalent to x = x / 5
x %= 2; // equivalent to x = x % 2
```

#HSLIDE

### Increment Operator

#HSLIDE

The **increment** operator is used to increase an **integer**'s value by one, and is a commonly used C# operator. 

```
x++; //equivalent to x = x + 1
```
**For example:**
```
int x = 10;
x++;
Console.WriteLine(x);
//Outputs 11
```

#HSLIDE

### Prefix & Postfix Forms

#HSLIDE

The increment operator has two forms, **prefix** and **postfix**.

```
++x; //prefix
x++; //postfix
```

#HSLIDE

**Prefix** increments the value, and then proceeds with the expression.
**Postfix** evaluates the expression and then performs the incrementing.

**Prefix example:**
```
int x = 3;
int y = ++x;
// x is 4, y is 4
```

**Postfix example:**
```
int x = 3;
int y = x++;
// x is 4, y is 3
```

>The prefix example increments the value of x, and then assigns it to y.
The postfix example assigns the value of x to y, and then increments x.

#HSLIDE

### Decrement Operator

#HSLIDE

The **decrement** operator (--) works in much the same way as the increment operator, but instead of increasing the value, it decreases it by one.

```
--x; // prefix
x--; // postfix
```

#HSLIDE
