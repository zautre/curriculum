

#HSLIDE
##Classes & Objects
#HSLIDE
## Introduction
#HSLIDE
### Classes
As we have seen in the previous modules, built-in data types are used to store a single value in a declared variable. For example, `int` **x** stores an `integer` value in a variable named **x**.
#HSLIDE
In object-oriented programming, a **class** is a data type that defines a set of variables and methods for a declared **object**.
A class is like a **blueprint**. It defines the data and behavior for a type. A class definition starts with the keyword ***`class`*** followed by the class name. The class body contains the data and actions enclosed by curly braces.
```c
class BankAccount
{
  //variables, methods, etc.
}
```
#HSLIDE

>The class defines a data type for objects, but it is not an object itself.
An object is a concrete entity based on a class, and is sometimes referred
to as an instance of a class.

#HSLIDE
### Objects
Just as a built-in data type is used to declare multiple variables, a class can be used to declare multiple **objects**.
#HSLIDE
As an analogy, in preparation for a new building, the architect designs a blueprint, which is used as a basis for actually building the structure.
That same blueprint can be used to create multiple buildings.
#HSLIDE
Programming works in the same fashion. We define (design) a class that is the blueprint for creating objects.
In programming, the term **type** is used to refer to a class **name**: We're creating an object of a particular **type**.
Once we've written the class, we can create objects based on that class. Creating an object is called **instantiation**.
#HSLIDE

>An object is called an instance of a class.

#HSLIDE
Each object has its own characteristics.
The characteristics of an object are called **properties**.
Values of these properties describe the current state of an object. For example, a Person (an object of the class Person) can be 30 years old, male, and named Antonio.
#HSLIDE

>Let's move on and see how to create your own custom classes and objects! 

#HSLIDE
## Value & References Types
#HSLIDE
### Value Types
C# has two ways of storing data: by **reference** and by **value**.
The build-in data types, such as `int` and `double`, are used to declare variables that are **value types**. Their value is stored in memory in a location called the **stack**.
#HSLIDE
For example, the declaration and assignment statement `int` **x = 10**; can be thought of as:


| Stack  |      |
|----------|----|
| - |         |
| 10 | <--x |
| - |         |


>The value of the variable x is now stored on the stack.

#HSLIDE
### Reference Types
**Reference** types are used for storing objects. For example, when you create an object of a class, it is stored as a reference type.
Reference types are stored in a part of the memory called the **heap**.
#HSLIDE
When you instantiate an object, the data for that object is stored on the heap, while its heap memory address is stored on the stack.
That is why it is called a reference type.

|   |   Stack      |   | Heap |
|----------|-------|---|------|
|  |      -       |   |    -  |
| x---> | 10      |    |      |
| p1--->  |0x042b8|--->|Person object|
|  |    -          |   |   -   |

#HSLIDE
As you can see, the **p1** object of type Person on the stack stores the memory address of the heap where the actual object is stored.

>Stack is used for static memory allocation, which includes all your
value types, like x.
Heap is used for dynamic memory allocation, which includes custom objects,
that might need additional memory during the runtime of your program.

#HSLIDE
## Class Example
#HSLIDE
### Example of a Class
Let’s create a **Person** class: 
```c
class Person
{
  int age;
  string name;
  public void SayHi()
  {
    Console.WriteLine("Hi");
  }
}
```
#HSLIDE
You can include an **access modifier** for fields and methods (also called **members**) of a class. Access modifiers are keywords used to specify the accessibility of a member.
A member that has been defined *public* can be accessed from outside the class, as long as it's anywhere within the scope of the class object. That is why our **SayHi** *method* is declared *public*, as we are going to call it from outside of the class.
#HSLIDE

>You can also designate class members as private or protected. This will be
discussed in greater detail later in the course. If no access modifier is
defined, the member is private by default.

#HSLIDE
Now that we have our Person class defined, we can instantiate an object of that type in Main.
The **new** operator instantiates an object and returns a reference to its location:
#HSLIDE
```c
class Person {
  int age;
  string name;
  public void SayHi() {
    Console.WriteLine("Hi");
  }
}
static void Main(string[] args)
{
  Person p1 = new Person();
  p1.SayHi();
}
//Outputs "Hi"
```
The code above declares a Person object named **p1** and then calls its *public* **SayHi()** *method*.
#HSLIDE

>Notice the dot operator (.) that is used to access and call the method of
the object.

#HSLIDE

You can access all *public* members of a class using the dot operator.
Besides calling a *method*, you can use the dot operator to make an assignment when valid.
For example: 

```c
class Dog
{
  public string name;
  public int age;
}
static void Main(string[] args)
{
  Dog bob = new Dog();
  bob.name = "Bobby";
  bob.age = 3;
  Console.WriteLine(bob.age);
  //Outputs 3
}
```
#HSLIDE