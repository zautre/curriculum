## break and continue
#HSLIDE
### break
We saw the use of **break** in the switch statement.
Another use of **break** is in loops: When the **break** statement is encountered inside a loop, the loop is immediately terminated and the program execution moves on to the next statement following the loop body.
#HSLIDE
**For example:**
```c
int num = 0;
while (num < 20)
{
   if (num == 5)
     break;

   Console.WriteLine(num);
   num++;
}

/* Outputs:
0
1
2
3
4
*/
```
#HSLIDE

>If you are using nested loops (i.e., one loop inside another loop),
the break statement will stop the execution of the innermost loop and start executing the next line of code after the block.
#HSLIDE
### continue
The **continue** statement is similar to the **break** statement, but instead of terminating the loop entirely, it skips the current iteration of the loop and continues with the next iteration.
#HSLIDE
**For example:**
```c
for (int i = 0; i < 10; i++) {
  if (i == 5)
    continue;

  Console.WriteLine(i);
}
/* Outputs:
0
1
2
3
4
6
7
8
9
*/
```
#HSLIDE

>As you can see, number 5 is not printed, as the continue statement skips
the remaining statements of that iteration of the loop.

#HSLIDE
### Logical Operators
#HSLIDE
Logical operators are used to join multiple expressions and return **true** or **false**.


*  `&&` - is used for **AND** Operator. Example:  `y && y` 
*  `||` - is used for **OR** Operator. Example: `x || y` 
*  `!` - is used for **NOT** Operator. Example: `!x` 

#HSLIDE
The **AND** operator (&&) works the following way: 


| Left Operand  | Right Operand | Result  |  
|---|:---:|---:|
| false | false | **false** |
| false | true | **false** |
| true | false | **false** |
| true | true | **true** |

#HSLIDE
For example, if you wish to display text to the screen only if **age** is greater than 18 AND **money** is greater than 100: 
```c
int age = 42;
double money = 540;
if(age > 18 && money > 100) {
  Console.WriteLine("Welcome");
}
```
The AND operator was used to combine the two expressions.
#HSLIDE

>With the AND operator, both operands must be true for the entire expression to be true.
#HSLIDE
### AND
You can join more than two conditions: 
```c
int age = 42;
int grade = 75;
if(age > 16 && age < 80 && grade > 50) 
  Console.WriteLine("Hey there");
```
#HSLIDE

 >The entire expression evaluates to true only if
 all of the conditions are true.
#HSLIDE
### The OR Operator
The **OR** operator (||) returns **true** if any one of its operands is **true**.

| Left Operand  | Right Operand | Result  |  
|---|:---:|---:|
| false | false | **false** |
| false | true | **true** |
| true | false | **true** |
| true | true | **true** |

#HSLIDE
**For Example:**
```c
int age = 18;
int score = 85;
if (age > 20 || score > 50) {
    Console.WriteLine("Welcome");
}
```
#HSLIDE

>You can join any number of logical OR statements you want.
In addition, multiple OR and AND statements may be joined together.
#HSLIDE
### Logical NOT
The logical **NOT** (!) operator works with just a single operand, reversing its logical state. Thus, if a condition is **true**, the NOT operator makes it **false**, and vice versa.

 | Right Operand | Result  |  
|---:|---:|
| true | **false** |
| false | **true** |
#HSLIDE
```c
int age = 8;
if ( !(age >= 16) ) {
  Console.Write("Your age is less than 16");
}

// Outputs "Your age is less than 16"
```
#HSLIDE
### The Conditional Operator
#HSLIDE
The ? : Operator
 
```c
int age = 42;
string msg;
if(age >= 18)
  msg = "Welcome";
else
  msg = "Sorry";

Console.WriteLine(msg);
```
This can be done in a more elegant and shorter way by using the **?: operator**: 
```c
int age = 42;
string msg;
msg = (age >= 18) ? "Welcome" : "Sorry";
Console.WriteLine(msg);
```
#HSLIDE
### Basic Calculator
#HSLIDE

Now let's create a simple project that repeatedly asks the user to enter two values and then displays their sum, until the user enters exit.
We start with a **do-while** loop that asks the user for input and calculates the **sum**: 

#HSLIDE

```c
do {
  Console.Write("x = ");
  int x = Convert.ToInt32(Console.ReadLine());

  Console.Write("y = ");
  int y = Convert.ToInt32(Console.ReadLine());

  int sum = x+y;
  Console.WriteLine("Result: {0}", sum);
}
while(true);
```
#HSLIDE

This code will ask for user input infinitely. Now we need to handle the "exit".

>If the user enters a non-integer value, the program
will crash from a conversion error. We will learn how to
handle errors like that in the coming modules.

#HSLIDE

If the user enters "exit" as the value of x, the program should quit the loop. To do this, we can use a **break** statement:
```c
Console.Write("x = ");
string str = Console.ReadLine();
if (str == "exit")
  break;

int x = Convert.ToInt32(str);
```

#HSLIDE

Here we compare the input with the value "exit" and break the loop.
So the whole program looks like:
```c
 do {
  Console.Write("x = ");
  string str = Console.ReadLine();
  if (str == "exit")
    break;

  int x = Convert.ToInt32(str);

  Console.Write("y = ");
  int y = Convert.ToInt32(Console.ReadLine());

  int sum = x + y;
  Console.WriteLine("Result: {0}", sum);
}
while (true);
```