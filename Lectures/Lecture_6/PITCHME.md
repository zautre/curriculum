## Passing Arguments
#HSLIDE
There are three ways to pass arguments to a *method* when the *method* is called: By **value**, By **reference**, and as **Output**.
#HSLIDE
By **value** copies the ***argument's*** value into the *method*'s formal parameter. Here, we can make changes to the parameter within the *method* without having any effect on the ***argument***.

>By default, C# uses call by value to pass arguments!

#HSLIDE
The following example demonstrates by value: 
```c
static void Sqr(int x)
{
  x = x * x;
}
static void Main()
{
  int a = 3;
  Sqr(a);
  Console.WriteLine(a); // Outputs 3
}
```
In this case, `x` is the parameter of the `Sqr` *method* and a is the actual `argument` passed into the *method*.
#HSLIDE

>As you can see, the Sqr method does not change the original value of
the variable, as it is passed by value, meaning that it operates on
the value, not the actual variable.

#HSLIDE
### Passing by Reference
Pass by **reference** copies an ***argument's*** memory address into the formal parameter. Inside the *method*, the address is used to access the actual ***argument*** used in the call. This means that changes made to the parameter affect the ***argument***.
#HSLIDE
To pass the value by reference, the `ref` keyword is used in both the call and the *method* definition: 
```c
static void Sqr(ref int x)
{
  x = x * x;
}
static void Main()
{
  int a = 3;
  Sqr(ref a);
  Console.WriteLine(a); // Outputs 9
}
```
The `ref` keyword passes the memory address to the *method* parameter, which allows the *method* to operate on the actual variable.
#HSLIDE

>The ref keyword is used both when defining the method and when calling it.

#HSLIDE
### Passing by Output
**Output** parameters are similar to reference parameters, except that they transfer data out of the *method* rather than accept data in. They are defined using the **out** keyword.
The variable supplied for the output parameter need not be initialized since that value will not be used.
#HSLIDE
**For example:** 
```c
static void GetValues(out int x, out int y)
{
  x = 5;
  y = 42;
}
static void Main(string[] args)
{
  int a, b;
  GetValues(out a, out b);
  //Now a equals 5, b equals 42
}
```
Unlike the previous reference type example, where the value 3 was referred to the *method*, which changed its value to 9, output parameters get their value from the *method* (5 and 42 in the above example).
#HSLIDE

>Similar to the ref keyword, the out keyword is used both when defining
the method and when calling it.

#HSLIDE
## Method Overloading
#HSLIDE
### Overloading
Method **overloading** is when multiple *methods* have the **same name**, but **different parameters**.
#HSLIDE
For example, you might have a **Print** *method* that outputs its parameter to the console window: 
```c
void Print(int a)
{
  Console.WriteLine("Value: "+a);
}
```
#HSLIDE
The + operator is used to concatenate values. In this case, the value of `a` is joined to the text "Value: ".
This *method* accepts an **integer argument** only.
#HSLIDE
Overloading it will make it available for other types, such as `double`:
```c
void Print(double a)
{
  Console.WriteLine("Value: "+a);
}
```

>Now, the same Print method name will work for both integers and doubles.

#HSLIDE
When overloading methods, the definitions of the methods must differ from each other by the types and/or number of parameters.
```c
static void Print(int a) {
  Console.WriteLine("Value: " + a);
}
static void Print(double a) {
  Console.WriteLine("Value: " + a);
}
static void Print(string label, double a) {
  Console.WriteLine(label + a);
}
static void Main(string[] args) {
  Print(11);
  Print(4.13);
  Print("Average: ", 7.57);
}
```
#HSLIDE

>You cannot overload method declarations that differ only by return type.
The following declaration results in an error.
```c
int PrintName(int a) { }
float PrintName(int b) { }
double PrintName(int c) { }
```

#HSLIDE
## Recursion
#HSLIDE
A **recursive** *method* is a *method* that calls itself.
#HSLIDE
One of the classic tasks that can be solved easily by recursion is calculating the **factorial** of a number.
In mathematics, the term **factorial** refers to the product of all positive integers that are less than or equal to a specific non-negative `integer` (n). The factorial of n is denoted as `n!`
#HSLIDE
For example:
```c
4! = 4 * 3 * 2 * 1 = 24
```
As you can see, a factorial can be thought of as repeatedly calculating num * num-1 until you reach 1.
#HSLIDE
Based on this solution, let's define our *method*: 
```c
static int Fact(int num) {
  if (num == 1) {
    return 1;
  }
  return num * Fact(num - 1);
}
```
#HSLIDE

Now we can call our Fact *method* from Main: 
```c
static void Main(string[] args)
{
  Console.WriteLine(Fact(6));
  //Outputs 720
}
```
#HSLIDE

>The factorial method calls itself, and then continues to do so,
until the argument equals 1. The exit condition prevents the method
from calling itself indefinitely.

#HSLIDE
## Making a Pyramid
#HSLIDE
Now, let's create a *method* that will display a pyramid of any height to the console window using star (*) symbols.
Based on this description, a parameter will be defined to reflect the number of rows for the pyramid.
#HSLIDE
So, let's start by declaring the *method*: 
```c
static void DrawPyramid(int n)
{
   //some code will go here
}
```
#HSLIDE
**DrawPyramid** does not need to return a value and takes an `integer` parameter `n`.
In programming, the step by step logic required for the solution to a problem is called an **algorithm**. The algorithm for MakePyramid is:
1. The first row should contain one star at the top center of the pyramid. The center is calculated based on the number of rows in the pyramid.
2. Each row after the first should contain an odd number of stars (1, 3, 5, etc.), until the number of rows is reached.

#HSLIDE
Based on the algorithm, the code will use for loops to display spaces and stars for each row: 
```c
static void DrawPyramid(int n)
{
  for (int i = 1; i <= n; i++)
  {
    for (int j = i; j <= n; j++)
    {
      Console.Write("  ");
    }
    for (int k = 1; k <= 2 * i - 1; k++)
    {
      Console.Write("*" + " ");
    }
    Console.WriteLine();
  }
}
```
#HSLIDE

>Now, if we call the DrawPyramid method, it will display a pyramid having the number of rows we pass to the method.