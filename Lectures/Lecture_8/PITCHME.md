
## Encapsulation
#HSLIDE
In programming, encapsulation means more than simply combining members together within a class; it also means restricting access to the inner workings of that class.
Encapsulation is implemented by using **access modifiers**. An access modifier defines the scope and visibility of a class member.
#HSLIDE

>Encapsulation is also called information hiding.

#HSLIDE
C# supports the following access modifiers: `public`, `private`, `protected`, **internal**, `protected` **internal**.
As seen in the previous examples, the `public` access modifier makes the member accessible from the outside of the class.
The `private` access modifier makes members accessible only from within the class and hides them from the outside.
#HSLIDE

To show encapsulation in action, let’s consider the following example: 
```c
class BankAccount
{
  private double balance=0;
  public void Deposit(double n)
  {
    balance += n;
  }
  public void Withdraw(double n)
  {
    balance -= n;
  }
  public double GetBalance()
  {
    return balance;
  }
}
```
#HSLIDE
You cannot directly change the **balance** variable. You can only view its value using the `public` *method*. This helps maintain data integrity.
We could add different verification and checking mechanisms to the methods to provide additional security and prevent errors.

#HSLIDE
>In summary, the benefits of encapsulation are:
- Control the way data is accessed or modified.
- Code is more flexible and easy to change with new requirements.
- Change one part of code without affecting other parts of code.

#HSLIDE
## Constructors
#HSLIDE
A class **constructor** is a special member *method* of a class that is executed whenever a new object of that class is created.
A **constructor** has exactly the same name as its class, is `public`, and does not have any return type.
#HSLIDE
For example:
```c
class Person
{
  private int age;
  public Person()
  {
    Console.WriteLine("Hi there");
  }
}
```
 Now, upon the creation of an object of type Person, the **constructor** is automatically called. 
#HSLIDE
```c
static void Main(string[] args)
{
  Person p = new Person();
}
// Outputs "Hi there"
```

>This can be useful in a number of situations. For example, when creating an object of type BankAccount, you could send an email notification
to the owner.
The same functionality could be achieved using a separate public method. The advantage of the constructor is that it is called automatically.

#HSLIDE
Constructors can be very useful for setting initial values for certain member variables.
A default **constructor** has no parameters.
#HSLIDE
However, when needed, parameters can be added to a **constructor**. 
This makes it possible to assign an initial value to an object when it's created, as shown in the following example: 
```c
class Person
{
  private int age;
  private string name;
  public Person(string nm)
  {
    name = nm;
  }
  public string getName()
  {
    return name;
  }
}
static void Main(string[] args)
 {
  Person p = new Person("David");
  Console.WriteLine(p.getName());
}
//Outputs "David"
```
#HSLIDE
Now, when the object is created, we can pass a parameter that will be assigned to the `name` variable.

>Constructors can be overloaded like any method by using different numbers of parameters.

#HSLIDE
## Properties
#HSLIDE
A **`property`** is a member that provides a flexible mechanism to read, write, or compute the value of a `private` field.
Properties can be used as if they are `public` data members, but they actually include special methods called **accessors**.
#HSLIDE
The **accessor** of a `property` contains the executable statements that help in getting (reading or computing) or setting (writing) a corresponding field. Accessor declarations can include a **get** *accessor*, a **set** *accessor*, or both.
#HSLIDE
For example: 
```c
class Person
{
  private string name; //field
  public string Name //property
  {
    get { return name; }
    set { name = value; }
  }
}
```
#HSLIDE

>***value*** is a special keyword, which represents the value we assign to
a **property** using the set accessor.
The name of the property can be anything you want, but coding
conventions dictate properties have the same name as the private field with a capital letter.

#HSLIDE
Once the `property` is defined, we can use it to assign and read the `private` member: 
```c
class Person
{
  private string name;
  public string Name
  {
    get { return name; }
    set { name = value; }
  }
}
static void Main(string[] args)
{
  Person p = new Person();
  p.Name = "Bob";
  Console.WriteLine(p.Name);
}
```
#HSLIDE

>The property is accessed by its name, just like any other public member of
the class.

#HSLIDE
Any *accessor* of a `property` can be omitted.
For example, the following code creates a `property` that is read-only:
```c
class Person
{
  private string name;
  public string Name
  {
    get { return name; }
  }
}
```
#HSLIDE

>A property can also be private, so it can be called only from within the
class.

#HSLIDE

>You can have any custom logic with get and set accessors.

#HSLIDE
### Auto-Implemented Properties
When you do not need any custom logic, C# provides a fast and effective mechanism for declaring `private` members through their properties.
#HSLIDE
For example, to create a `private` member that can only be accessed through the **Name** `property`'s **get** and **set** accessors, use the following syntax: 
```c
public string Name { get; set; }
```
#HSLIDE
As you can see, you do not need to declare the `private` field name separately - it is created by the `property` automatically. **Name** is called an **auto-implemented** **`property`**. Also called auto-properties, they allow for easy and short declaration of `private` members.
#HSLIDE
We can rewrite the code from our previous example using an auto-`property`:
```c
class Person
{
  public string Name { get; set; }
}
static void Main(string[] args)
{
  Person p = new Person();
  p.Name = "Bob";
  Console.WriteLine(p.Name);
}
// Outputs "Bob"
```
#HSLIDE